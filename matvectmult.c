#include "matvect.h"
#include <time.h>
#include <math.h>
#include <memory.h>
#include <unistd.h>
#include <string.h>



int main(int argc, char *argv [])
{
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);



    if(argc < 6)
    {
        if(rank == MASTER)
        {
            printf("Too less arguments. Usage matvectmult m n mt vt at\nWhere:\n\tm - how many rows\n\tn - how many columns\n\tmt - matrix type\n\tvt - vector type\n\tat - algorithm type\n");
            printf("\nMatrix/vector types are:\n\ti - id vector/matrix\n\trandom - random vector/matrix\n\tgrow - growing vector/matrix\n");
            printf("\nAlgorithm types are:\n\ts - sequential algorithm\n\tp - parallel algorithm\n");
        }
        MPI_Finalize();
        return 0;
    }
    if(size < 2 && (strcmp(argv[5], "p") == 0))
    {
        if(rank == MASTER)
        {
            printf("Too less processes for parallel algorithm. At least 2 is needed. Usage matvectmult m n mt vt at\nWhere:\n\tm - how many rows\n\tn - how many columns\n\tmt - matrix type\n\tvt - vector type\n\tat - algorithm type\n");
            printf("\nMatrix/vector types are:\n\ti - id vector/matrix\n\trandom - random vector/matrix\n\tgrow - growing vector/matrix\n");
            printf("\nAlgorithm types are:\n\ts - sequential algorithm\n\tp - parallel algorithm\n");
        }
        MPI_Finalize();
        return 0;
    }


    size_t m, n;
    double elapsed_time = 0.0;
    char benchFileName[255] = {0};
    FILE * benchFile;

    if(rank == MASTER)
    {
        sprintf(benchFileName, "bench_%s_%s_%s_%s.txt", argv[3], argv[4], argv[1], argv[2]);
        benchFile = fopen(benchFileName, "a");
    }

    char matrixToLoadName[255] = {0};
    char vectorToLoadName[255] = {0};

    sprintf(matrixToLoadName, "data/%s_matrix_%s_%s.dat", argv[3], argv[1], argv[2]);
    sprintf(vectorToLoadName, "data/%s_vector_%s.dat", argv[4], argv[2]);

    if(rank == MASTER)
    {
        if(access(matrixToLoadName, F_OK) == -1)
        {
            printf("Not yet generated data files, generating them now...\n");
            int tmpM = atoi(argv[1]);
            int tmpN = atoi(argv[2]);
            generate_random_data_package(tmpM, tmpN);
        }
    }

    if(strcmp(argv[5], "p") == 0)
    {
        MPI_Barrier(MPI_COMM_WORLD);
        if(rank == MASTER)
        {
            printf("Starting parallel algorithm...\r\n");
        }   
        double **M;
        read_row_matrix(matrixToLoadName, MPI_DOUBLE, &m, &n, &M, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
        //print_row_matrix(M, MPI_DOUBLE, m, n, MPI_COMM_WORLD); //verify
        
        MPI_Barrier(MPI_COMM_WORLD);
        double *b;
        read_vector_and_replicate(vectorToLoadName, MPI_DOUBLE, &n, &b, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);

        double * fullC;
        elapsed_time = -MPI_Wtime();
        par_matvec_mult_lf(M, b, m, n, &fullC);
        MPI_Barrier(MPI_COMM_WORLD);
        elapsed_time += MPI_Wtime();
        
        if ( rank == 0)
        {
            testVectPrintf(fullC, m);
            printf ("Elapsed time: %2f s\n", elapsed_time );
            fprintf(benchFile , "%d;%2f \r\n", size, elapsed_time); // store time result for plotting
            fclose(benchFile);
            
        }

        vector_free(fullC);
        vector_free(b);
        matrix_free(M);
    }
    else if(strcmp(argv[5], "s") == 0)
    {
        printf("Starting sequential algorithm...\r\n");
        int tmpM = atoi(argv[1]);
        int tmpN = atoi(argv[2]);
        double ** M = matrix_alloc(tmpM, tmpN, sizeof(double));
        double * V = vector_alloc(tmpN, sizeof(double));
        double * c;
        read_matrix(matrixToLoadName, sizeof(double), &m, &n, &M);
        read_vector(vectorToLoadName, sizeof(double), &n, &V);

        elapsed_time = -MPI_Wtime();
        seq_matvec_mult_lf(M, V, m, n, &c);
        elapsed_time += MPI_Wtime();

        testVectPrintf(c, m);
        printf ("Elapsed time: %2f s\n", elapsed_time );
        fprintf(benchFile , "%d;%2f \r\n", size, elapsed_time);

        matrix_free(M);
        vector_free(V);
        vector_free(c);

        fclose(benchFile);
    }
    else
    {
        if(rank == MASTER)
        {
            printf("Bad algorithm type. Usage matvectmult m n mt vt at\nWhere:\n\tm - how many rows\n\tn - how many columns\n\tmt - matrix type\n\tvt - vector type\n\tat - algorithm type\n");
            printf("\nMatrix/vector types are:\n\ti - id vector/matrix\n\trandom - random vector/matrix\n\tgrow - growing vector/matrix\n");
            printf("\nAlgorithm types are:\n\ts - sequential algorithm\n\tp - parallel algorithm\n");
        }
        fclose(benchFile);
        MPI_Finalize();
        return 0;
        
    }

    
    MPI_Finalize();
    return 0;
}
