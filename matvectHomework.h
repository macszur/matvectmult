//
//  matvectHomework.h
//  parallelHomework
//
//  Created by PrzemcioMacP on 17.11.2014.
//  Copyright (c) 2014 macszur. All rights reserved.
//

#ifndef __parallelHomework__matvectHomework__
#define __parallelHomework__matvectHomework__

#include <stdio.h>
#include <stdlib.h>

void matrixAllocTest();
void vectorAllocTest();
void generateVectorAndMatrices();

// vector/matrix funs



#endif /* defined(__parallelHomework__matvectHomework__) */
