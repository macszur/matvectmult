//
//  matvect.c
//  parallelHomework
//
//  Created by PrzemcioMacP on 21.11.2014.
//  Copyright (c) 2014 macszur. All rights reserved.
//
#define min(a,b) (((a)>(b))?(a):(b))
#include "matvect.h"

int size = 0;
int rank = 0;
MPI_Status status;

void** matrix_alloc(size_t m, size_t n, size_t size )
{
    char* Astorage = (char*)malloc(size * m * n);
    void** A = (void**)malloc(sizeof(void*) * m);
    
    for(size_t i = 0; i < m; ++i)
    {
        *(A + i) = (Astorage + n * i*size);
    }
    return A;
}

void matrix_free(void** M)
{
    // firstly free Astorage
    free(*M);
    // then free A
    free(M);
    //assert(0);
}

void store_matrix(char * f, size_t m, size_t n, size_t size, void** M)
{
    char** dataPtr = (char**)M;
    FILE * saveF = fopen(f, "wb");
    fprintf(saveF, "%zu %zu", m, n);
    fwrite(*dataPtr, size, m*n, saveF);
    fclose(saveF);
}

void read_matrix(char * f, size_t size , size_t * m, size_t * n, void *** M)
{
    FILE * readF = fopen(f, "rb");
    fscanf(readF, "%zd %zd", m, n);
    fread(**M, size, (*m)*(*n), readF);
    fclose(readF);
}

void store_int_matrix(char * f, size_t m, size_t n, size_t size, void** M)
{
    int** tmpPtr = (int**)M;
    FILE * saveF = fopen(f, "w");
    fprintf(saveF, "%zu %zu", m, n);
    for(int i = 0; i < m; ++i)
    {
        for(int y = 0; y < n; ++y)
        {
            fprintf(saveF, " %d", tmpPtr[i][y]);
        }
    }
    fprintf(saveF, "\n");
    fclose(saveF);
}

double * generate_base_double_vector(size_t n, int id)
{
    double * x = vector_alloc(n, sizeof(double));
    for (int i = 0; i < n; i++)
    {
        x[i] = 0;
    }
    x[id] = 1;
    return x;
}

double * generate_random_double_vector(size_t n)
{
    double * x = vector_alloc(n, sizeof(double));
    for (int i = 0; i < n; i++)
    {
        x[i] = random();
    }
    return x;
}

double ** generate_identity_double_matrix(size_t m, size_t n)
{
    double ** buff = (double**) matrix_alloc(m, n, sizeof(double));
    
    for(int i = 0; i < m; ++i)
    {
        for(int j = 0; j < n; ++j)
        {
            if((i  == j) && (i < min(m, n)))
            {
                buff[i][j] = 1;
            }
            else
                buff[i][j] = 0;
        }
    }
    
    return buff;
}

double ** generate_random_double_matrix(size_t m, size_t n)
{
    srandom((unsigned int)time(NULL));
    double ** buff = (double**)matrix_alloc(m, n, sizeof(double));
    for(int i = 0; i < m*n; ++i)
    {
        *(*(buff) + i) = random();
    }
    
    
    return buff;
}

//void print_matrix_lf(void** M, MPI_Datatype type, size_t m, size_t n)
//{
//
//}

void* vector_alloc(size_t n, size_t size)
{
    return malloc(n * size);
}

void vector_free(void* v)
{
    free(v);
}

void store_vector(char * f, size_t n, size_t size , void * v)
{
    FILE* saveF = fopen(f, "wb");
    if(saveF == NULL)
	perror("FATAL FAILURE");
    printf("n count  %d\n", n);
    fprintf(saveF, "%d", n);
    fwrite(v, size, n, saveF);
    fclose(saveF);
}

void read_vector(char * f, size_t size, size_t * n, void ** v)
{
    FILE* openF = fopen(f, "rb");
    fscanf(openF, "%zd", n);
    *v = vector_alloc(*n, size);
    fread(*v, size, *n, openF);
    fclose(openF);
}

void seq_matvec_mult_lf(double ** M, double *b, size_t m, size_t n, double **c)
{
    *c = vector_alloc(m, sizeof(double));
    printf("Vector alloc size: %zd\n", n);
    for(int i = 0; i < m; i++)
    {
        double s = 0;
        for(int j = 0; j < n; j++)
        {
            s += M[i][j] * b[j];
        }
        (*c)[i] = s;
    }
}

void create_mixed_count_disp_arrays(int p, size_t k, int **count, int **disp) 
{
    //printf("creating mixed count and disp arrays, p=%d, k=%d\r\n", p, k);
    *count = (int*)malloc(p * sizeof(int));
    *disp = (int*)malloc(p * sizeof(int));
    for(int i = 0; i < p; ++i)
    {
        (*count)[i] = BLOCK_SIZE(i, p, k);
        if(i == 0)
            (*disp)[0] = 0;
        else
            (*disp)[i] = (*disp)[i-1] + (*count)[i-1];

        //printf("count[%d]=%d\r\ndisp[%d]=%d\r\n", i, (*count)[i], i, (*disp)[i]);
    }
    //printf("created mixed count and disp arrays\r\n");
}

void create_count_disp_arrays(int id, int p, size_t k, int **count, int **disp)
{
    
}

void testVectFill(void* v, size_t n)
{
    double * intBuff = (double*)v;
    for(int i = 0; i < n; ++i)
    {
        *(intBuff + i) = i;
    }
}

void testVectPrintf(void* v, size_t n)
{
    double* intMatBuff = (double*)v;
    
    //printf("Now with vector style \r\n");
    for(int i = 0; i < n; ++i)
    {
            printf("Vect[%d] : %f\n", i, intMatBuff[i]);
    }
}

void testFill(void* buff, int m, int n)
{
    double * intBuff = (double*)buff;
    for(int i = 0; i < n*m; ++i)
    {
        *(intBuff + i) = i;
    }
}

void testPrintf(void** matBuff, int m, int n)
{
    int** intMatBuff = (int**)matBuff;
    
    //printf("Now with array style \r\n");
    for(int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j) {
            printf("Arr[%d][%d] : %d\n", i, j, intMatBuff[i][j]);
        }
    }
}

void testDoublePrintf(double** matBuff, int m, int n)
{
    double** intMatBuff = (double**)matBuff;
    
    //printf("Now with array style \r\n");
    for(int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j) {
            printf("Arr[%d][%d] : %lf\n", i, j, intMatBuff[i][j]);
        }
    }
}


void generate_random_data_package(size_t m, size_t n)
{
    char filename[255];

    printf("generating 2,3,4,5... vector for n=%zd\n", n);
    snprintf(filename, 100, "data/grow_vector_%zd.dat", n);
    double * g = vector_alloc(n, sizeof(double));
    testVectFill(g, n);
    store_vector(filename, n, sizeof(double), g);
    vector_free(g);

    printf("generating random vector for n=%zd\n", n);
    snprintf(filename, 255, "data/random_vector_%zd.dat", n);
    double * b = generate_random_double_vector(n);
    store_vector(filename, n, sizeof(double), b);
    vector_free(b);

    printf("generating id matrix m=%zd n=%zd \n", m, n);
    snprintf(filename, 255, "data/id_matrix_%zd_%zd.dat", m, n);
    double ** c = generate_identity_double_matrix(m, n);
    store_matrix(filename, m, n, sizeof(double), c);
    matrix_free(c);
    
    printf("generating random matrix m=%zd n=%zd\n", m, n);
    snprintf(filename, 255, "data/random_matrix_%zd_%zd.dat", m, n);
    double ** d = generate_random_double_matrix(m, n);
    store_matrix(filename, m, n, sizeof(double), d);
    matrix_free(d);

    printf("generating 2,3,4,5... matrix for m=%zd n=%zd\n", m, n);
    snprintf(filename, 255, "data/grow_matrix_%zd_%zd.dat", m, n);
    double ** gm = matrix_alloc(m, n, sizeof(double));
    testFill(*gm, m, n);
    store_matrix(filename, m, n, sizeof(double), gm);
    matrix_free(gm);
}

//-------------------------------ROWWISE DIVISION-----------------------------------------------------------------

void read_row_matrix(char * f, MPI_Datatype dtype, size_t * m, size_t * n, void *** M, MPI_Comm comm)
 {
    FILE * readF;
    if (rank == MASTER)
    {
        readF = fopen(f, "rb");
        fscanf(readF, "%zd %zd", m, n);
        //printf("m=%zd n=%zd\r\n", *m, *n);
    } 

    int tmp = (int)*m;    
    MPI_Bcast(&tmp, 1, MPI_INT, MASTER, comm);
    *m = (size_t)tmp;
    tmp = (int)*n;
    MPI_Bcast(&tmp, 1, MPI_INT, MASTER, comm);
    *n = (size_t)tmp;

    //printf("%d BLOCK_SIZE:%d\r\n", rank, BLOCK_SIZE(rank, size, *m));
    *M = matrix_alloc((size_t)BLOCK_SIZE(rank, size, *m), *n, sizeof(double));
    //double *** dM = (double***)M;
    if (rank == MASTER) {
        void **H = matrix_alloc(BLOCK_SIZE(size-1, size, *m), *n, sizeof(double));
        //double **dH = (double**)H;
        for (int i = 0; i < size; i++)
        {
            if (i == MASTER)
            {
                fread(**M, sizeof(double), BLOCK_SIZE(MASTER, size, *m) * *n, readF);
                // for(int z = 0; z < BLOCK_SIZE(MASTER, size, *m) * *n; ++z)
                // {
                //     printf("PARSED_master[%d] : %f\n", z, *((**dM) + z));
                // }
            } 
            else
            {
                fread(*H, sizeof(double), BLOCK_SIZE(i, size, *m) * *n, readF);
                // for(int z = 0; z < BLOCK_SIZE(i, size, *m) * *n; ++z)
                // {
                //     printf("PARSED_proc_%d[%d] : %f\n", i, z, *((*dH) + z));
                // }
                MPI_Send(*H, (size_t)BLOCK_SIZE(i, size, *m) * *n, dtype, i, 0, comm);
            }
        }
        matrix_free(H);
        fclose(readF);
    } 
    else
    {
        MPI_Recv(**M, (size_t)BLOCK_SIZE(rank, size, *m) * *n, dtype, MASTER, 0, comm, &status);
        // for(int z = 0; z < BLOCK_SIZE(rank, size, *m) * *n; ++z)
        // {
        //     printf("RECIVED_proc_%d[%d] : %f\n", rank, z, *((**dM) + z));
        // }
        //testDoublePrintf(*M, 2, 4);
    }
}

void read_vector_and_replicate(char * f, MPI_Datatype dtype, size_t * n, void ** v, MPI_Comm comm)
{

    if (rank == MASTER) {
        read_vector(f, sizeof(double), n, v);
    }

    int tmp = (int)*n;
    MPI_Bcast(&tmp, 1, MPI_INT, MASTER, comm);
    *n = (size_t)tmp;

    if (rank == MASTER) {
        for (int i = 0; i < size; i++) {
            if (i != MASTER) {
                MPI_Send(*v, *n, dtype, i, 0, comm);
            }
        }
    } 
    else
    {
        *v = vector_alloc(*n, sizeof(double));
        MPI_Recv(*v, *n, dtype, MASTER, 0, comm, &status);
    }

}

void print_row_vector(void * v, MPI_Datatype type, size_t n, MPI_Comm comm)
{
    double * fullVector;
    int * countRecvArr;
    int * displacementArr;

    if(rank == MASTER)
    {
        fullVector = vector_alloc(n, sizeof(double));
        create_mixed_count_disp_arrays(size, n, &countRecvArr, &displacementArr);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Gatherv(v, BLOCK_SIZE(rank, size, n), type, fullVector, countRecvArr, displacementArr, type, MASTER, comm);

    if(rank == MASTER)
    {
        testVectPrintf(fullVector, n);
        free(countRecvArr);
        free(displacementArr);
        free(fullVector);
    }
}

void print_row_matrix(void ** M, MPI_Datatype type, size_t m, size_t n, MPI_Comm comm)
{
    double ** fullMatrix;
    int * countRecvArr;
    int * displacementArr;
    MPI_Barrier(comm);

    if(rank == MASTER)
    {
        fullMatrix = matrix_alloc(m, n, sizeof(double));
        create_mixed_count_disp_arrays(size, m, &countRecvArr, &displacementArr);
        for(int i = 0; i < size; ++i)
        {
            countRecvArr[i] *= (int)n;
            displacementArr[i] *= (int)n;
        }
    }

    MPI_Barrier(comm);
    MPI_Gatherv(*M, BLOCK_SIZE(rank, size, m) * n, type, *fullMatrix, countRecvArr, displacementArr, type, MASTER, comm);

    if(rank == MASTER)
    {
        testDoublePrintf(fullMatrix, m, n);
        free(countRecvArr);
        free(displacementArr);
        matrix_free(fullMatrix);
    }

}

void par_matvec_mult_lf(double ** M, double * b, size_t m, size_t n, double **cc)
{
    double *c = vector_alloc(BLOCK_SIZE(rank, size, m), sizeof(double));
    *cc = vector_alloc(m, sizeof(double));
    int * countRecvArr;
    int * displacementArr;
    create_mixed_count_disp_arrays(size, m, &countRecvArr, &displacementArr);

    MPI_Barrier(MPI_COMM_WORLD);
    for(int i = 0; i < BLOCK_SIZE(rank, size, m); i++)
    {
        double s = 0;
        for(int j = 0; j < n; j++)
        {
            s += M[i][j] * b[j];
        }
        //printf("%d c[%d] = %lf\n", rank, i, s);
        c[i] = s;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Allgatherv(c, BLOCK_SIZE(rank, size, m), MPI_DOUBLE, *cc, countRecvArr, displacementArr, MPI_DOUBLE, MPI_COMM_WORLD);
}