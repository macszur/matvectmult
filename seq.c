#include "matvect.h"

int main() 
{
    const int M = 100;
    const int N = 50;
    
    double ** A = generate_identity_double_matrix(M, N);
    double * b = generate_random_double_vector(N);
    double * c;
    seq_matvec_mult_lf(A, b, M, N, &c);

    for (int i = 0; i < N; i++) {
        printf("%lf %lf\n", b[i], c[i]);
    }

    matrix_free(A);
    vector_free(b);
    vector_free(c);

    return 0;
}
