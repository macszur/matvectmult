#include "matvect.h"

size_t m, n;
char filename[100];

int main() 
{
    printf("Type m and n value: ");
    scanf("%zd %zd", &m, &n);
    
    // printf("generating base vector for n=%zd\n", n);
    // for (int i = 0; i < n; i++) {
    //     snprintf(filename, 100, "data/base_%d_vector_n.dat", i);
    //     double * a = generate_base_double_vector(n, i);
    //     store_vector(filename, n, sizeof(double), a);
    //     vector_free(a);
    // }

    printf("generating 2,3,4,5... vector for n=%zd\n", n);
    snprintf(filename, 100, "data/grow_vector_%zd.dat", n);
    double * g = vector_alloc(n, sizeof(double));
    testVectFill(g, n);
    store_vector(filename, n, sizeof(double), g);
    vector_free(g);

    printf("generating random vector for n=%zd\n", n);
    snprintf(filename, 100, "data/random_vector_%zd.dat", n);
    double * b = generate_random_double_vector(n);
    store_vector(filename, n, sizeof(double), b);
    vector_free(b);

    printf("generating id matrix m=%zd n=%zd \n", m, n);
    snprintf(filename, 100, "data/id_matrix_%zd_%zd.dat", m, n);
    double ** c = generate_identity_double_matrix(m, n);
    store_matrix(filename, m, n, sizeof(double), c);
    matrix_free(c);
    
    printf("generating random matrix m=%zd n=%zd\n", m, n);
    snprintf(filename, 100, "data/random_matrix_%zd_%zd.dat", m, n);
    double ** d = generate_random_double_matrix(m, n);
    store_matrix(filename, m, n, sizeof(double), d);
    matrix_free(d);

    printf("generating 2,3,4,5... matrix for m=%zd n=%zd\n", m, n);
    snprintf(filename, 100, "data/grow_matrix_%zd_%zd.dat", m, n);
    double ** gm = matrix_alloc(m, n, sizeof(double));
    testFill(*gm, m, n);
    store_matrix(filename, m, n, sizeof(double), gm);
    matrix_free(gm);

    return 0;
}
