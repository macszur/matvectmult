//
//  matvect.h
//  parallelHomework
//
//  Created by PrzemcioMacP on 21.11.2014.
//  Copyright (c) 2014 macszur. All rights reserved.
//

#ifndef __parallelHomework__matvect__
#define __parallelHomework__matvect__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

#define BLOCK_LOW(id,p,k) (((id)*((int)k))/(p))
#define BLOCK_HIGH(id,p,k) ((BLOCK_LOW((id)+1,p,k)-1))
#define BLOCK_SIZE(id,p,k) ((BLOCK_HIGH(id,p,k)-BLOCK_LOW(id,p,k)+1))
#define BLOCK_OWNER(j,p,k) ((((p)*((j)+1)−1)/((int)k)))

#define MASTER (0)

extern int rank, size;
extern MPI_Status status;

void** matrix_alloc(size_t m, size_t n, size_t size );
void matrix_free(void** M);
void store_matrix(char * f, size_t m, size_t n, size_t size, void** M);
void read_matrix(char * f, size_t size , size_t * m, size_t * n, void*** M);

double * generate_base_double_vector(size_t n, int id);
double * generate_random_double_vector(size_t n);
double ** generate_identity_double_matrix(size_t m, size_t n);
double ** generate_random_double_matrix(size_t m, size_t n);

void* vector_alloc(size_t n, size_t size);
void vector_free(void * v);
void store_vector(char * f, size_t n, size_t size , void * v);
void read_vector(char * f, size_t size, size_t * n, void ** v);

void seq_matvec_mult_lf(double ** M, double *b, size_t m, size_t n, double **c);

void create_mixed_count_disp_arrays(int p, size_t k, int **count, int **disp);
void create_count_disp_arrays(int id, int p, size_t k, int **count, int **disp);

void testPrintf(void** matBuff, int m, int n);
void testDoublePrintf(double** matBuff, int m, int n);
void testFill(void* buff, int m, int n);

void testVectFill(void *v, size_t n);
void testVectPrintf(void * v, size_t n);

void generate_random_data_package(size_t m, size_t n);

void read_row_matrix(char * f, MPI_Datatype dtype, size_t * m, size_t * n, void *** M, MPI_Comm comm);
void read_vector_and_replicate(char * f, MPI_Datatype dtype, size_t * n, void ** v, MPI_Comm comm);
void print_row_vector(void * v, MPI_Datatype type, size_t n, MPI_Comm comm);
void print_row_matrix(void ** M, MPI_Datatype type, size_t m, size_t n, MPI_Comm comm);

void par_matvec_mult_lf(double ** M, double * b, size_t m, size_t n, double **cc);


#endif /* defined(__parallelHomework__matvect__) */
