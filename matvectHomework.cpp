//
//  matvectHomework.cpp
//  parallelHomework
//
//  Created by PrzemcioMacP on 17.11.2014.
//  Copyright (c) 2014 macszur. All rights reserved.
//

#include "matvectHomework.h"
#include <assert.h>

extern "C" {
#include "matvect.h"
}


void matrixAllocTest()
{
    const int N = 5;
    const int M = 10;
    
    void** buff = matrix_alloc(5, 10, sizeof(int));
    testFill(*buff, 5, 10);
    testPrintf(buff, 5, 10);
    store_matrix("testMatBin.txt", N, M, sizeof(int), buff);
    void** buff2 = matrix_alloc(N, M, sizeof(int));
    size_t m = 1, n = 1;
    
    read_matrix("testMatBin.txt", sizeof(int), &m, &n, &buff2);
    testPrintf(buff2, m, n);
    matrix_free(buff);
    matrix_free(buff2);
    
}

void vectorAllocTest()
{
    const int N = 35;
    
    void* buff = vector_alloc(N, sizeof(int));
    testVectFill(buff, N);
    testVectPrintf(buff, N);
    store_vector("testVectBin.txt", N, sizeof(int), buff);
    void* buff2 = vector_alloc(N, sizeof(int));
    size_t n = 1;
    
    read_vector("testVectBin.txt", sizeof(int), &n, &buff2);
    testVectPrintf(buff2, n);
    vector_free(buff);
    vector_free(buff2);
    
}

void generateVectorAndMatrices()
{
    // take user input
    size_t m = 0, n = 0;
    m = 5; n = 5;
    // generate base vector
    // generate random vector
    // generate identity matrix
    double ** dBuff = generate_identity_double_matrix(m, n, sizeof(double));
    testDoublePrintf(dBuff, m, n);
    free(dBuff);
    
    // generate random matrix
    dBuff = generate_random_double_matrix(m, n);
    testDoublePrintf(dBuff, m, n);
    free(dBuff);
    
}


/*

*/