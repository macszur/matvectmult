//
//  main.cpp
//  parallelHomework
//
//  Created by PrzemcioMacP on 18.10.2014.
//  Copyright (c) 2014 macszur. All rights reserved.
//
#include "matvectHomework.h"
#include <iostream>

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Parallel and grid compution homeworks!\n";
    //piHomeworkRun();
    //matrixAllocTest();
    //vectorAllocTest();
    
    generateVectorAndMatrices();
    
    return 0;
}
