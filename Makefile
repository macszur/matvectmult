CC=mpicc
CFLAGS=-Wall --std=c99 -g3 -g
LDFLAGS=-D_XOPEN_SOURCE=600

SRC=matvect.c
TARGET=seq par gen

all: $(TARGET)

%: %.c $(SRC)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

clean:
	@echo "=> cleaning..."
	rm -rf $(TARGET)

